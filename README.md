# NANOAOD

This is old package I used to draw plots. 

Gridpacks generated previously are stored in tarball/. 

## Getting started

These files and directories should be put in CMSSW_10_6_19/src

And we shoud run these commands to generate NANOAOD: 

```
//Environment and Compilation
cd CMSSW_10_6_19/src
cmsenv
scram b

//Generate .py files
cmsDriver.py Configuration/GenProduction/python/wellnu01234j_5f_LO_MLM_MGv2.py --python_filename wellnu01234j_5f_LO_MLM_mg2.py --eventcontent NANOAODGEN --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAOD --fileout file:wellnu01234j_5f_LO_MLM_mg2.root --conditions 106X_mcRun2_asymptotic_preVFP_v8 --beamspot Realistic25ns13TeV2016Collision --step LHE,GEN,NANOGEN --geometry DB:Extended --era Run2_2016_HIPM --no_exec --mc -n 10000
cmsDriver.py Configuration/GenProduction/python/wellnu01234j_5f_LO_MLM_MGv3.py --python_filename wellnu01234j_5f_LO_MLM_mg3.py --eventcontent NANOAODGEN --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAOD --fileout file:wellnu01234j_5f_LO_MLM_mg3.root --conditions 106X_mcRun2_asymptotic_preVFP_v8 --beamspot Realistic25ns13TeV2016Collision --step LHE,GEN,NANOGEN --geometry DB:Extended --era Run2_2016_HIPM --no_exec --mc -n 10000
cmsDriver.py Configuration/GenProduction/python/wellnu012j_5f_NLO_FXFX_MGv2.py --python_filename wellnu012j_5f_NLO_FXFX_mg2.py --eventcontent NANOAODGEN --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAOD --fileout file:wellnu012j_5f_NLO_FXFX_mg2.root --conditions 106X_mcRun2_asymptotic_preVFP_v8 --beamspot Realistic25ns13TeV2016Collision --step LHE,GEN,NANOGEN --geometry DB:Extended --era Run2_2016_HIPM --no_exec --mc -n 10000
cmsDriver.py Configuration/GenProduction/python/wellnu012j_5f_NLO_FXFX_MGv3.py --python_filename wellnu012j_5f_NLO_FXFX_mg3.py --eventcontent NANOAODGEN --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAOD --fileout file:wellnu012j_5f_NLO_FXFX_mg3.root --conditions 106X_mcRun2_asymptotic_preVFP_v8 --beamspot Realistic25ns13TeV2016Collision --step LHE,GEN,NANOGEN --geometry DB:Extended --era Run2_2016_HIPM --no_exec --mc -n 10000

//Run these .py files
cmsRun wellnu01234j_5f_LO_MLM_mg2.py >& log_LO_mg2 &
cmsRun wellnu01234j_5f_LO_MLM_mg3.py >& log_LO_mg3 &
cmsRun wellnu012j_5f_NLO_FXFX_mg2.py >& log_NLO_mg2 &
cmsRun wellnu012j_5f_NLO_FXFX_mg3.py >& log_NLO_mg3 &
```

Four root files are generated. 

## Draw plots

Just run readtree.C: 

```
cd draw
root -l readtree.C
```

